{ nixpkgs ? import (fetchTarball "https://nixos.org/channels/nixos-18.03-small/nixexprs.tar.xz") { } }:
nixpkgs.pkgs.haskellPackages.callPackage ./bindings-gnunet.nix { gnunet = (nixpkgs.pkgs.callPackage ~/gnunet/gnunet-dev.nix { }).dev; }
