module Main where

import Control.Monad
import Control.Exception
import Foreign.Ptr
import Foreign.Marshal hiding (void)
import Foreign.C.String
import Bindings.GNUnet.GnunetCryptoLib
import Bindings.GNUnet.GnunetCryptoTypes
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetConfigurationLib
import Bindings.GNUnet.GnunetSchedulerLib
import Bindings.GNUnet.GnunetCadetService

assert' :: Monad m => Bool -> m ()
assert' = flip assert (return ())

channelIncoming' :: Ptr () -> Ptr C'GNUNET_CADET_Channel -> Ptr C'GNUNET_PeerIdentity -> IO (Ptr ())
channelIncoming' _ channel initiator = do
    -- TODO: Note we don't close_port, which results in assertion at cadet_api.c:1207
    i <- peekCString =<< c'GNUNET_i2s_full initiator
    withCString ("Incoming connection from " ++ i ++ "\n") $
        c'GNUNET_log_nocheck c'GNUNET_ERROR_TYPE_DEBUG

    -- TODO: would be nicer to be IO (Ptr C'GNUNET_CADET_Channel)
    return $ castPtr @_ @() channel

channelEnded' :: Ptr () -> Ptr C'GNUNET_CADET_Channel -> IO ()
channelEnded' _ _ = do
    withCString "Channel ended!\n" $ c'GNUNET_log_nocheck c'GNUNET_ERROR_TYPE_DEBUG
    c'GNUNET_SCHEDULER_shutdown

main :: IO ()
main = do
    name <- newCString "cadet-test"
    loglevel <- newCString "DEBUG"
    assert' =<< (== C'GNUNET_OK) <$> c'GNUNET_log_setup name loglevel nullPtr

    cfgpath <- newCString "gnunet.conf"
    cfg <- c'GNUNET_CONFIGURATION_create
    assert' =<< (/= C'GNUNET_SYSERR) <$> c'GNUNET_CONFIGURATION_load cfg cfgpath

    porthash <- malloc @C'GNUNET_HashCode

    channelIncoming <- mk'GNUNET_CADET_ConnectEventHandler channelIncoming'
    channelEnded <- mk'GNUNET_CADET_DisconnectEventHandler channelEnded'

    flip c'GNUNET_SCHEDULER_run nullPtr =<< (mk'GNUNET_SCHEDULER_TaskCallback . const $ do
        withCString "Connecting to CADET service\n" $ c'GNUNET_log_nocheck c'GNUNET_ERROR_TYPE_DEBUG
        mh <- c'GNUNET_CADET_connect cfg
        -- FIXME: needs better short-circuiting
        when (nullPtr == mh) $ c'GNUNET_SCHEDULER_shutdown >> undefined
        void . flip c'GNUNET_SCHEDULER_add_shutdown nullPtr =<< (mk'GNUNET_SCHEDULER_TaskCallback . const $ c'GNUNET_CADET_disconnect mh)

        withCString "q" $ \s ->
            c'GNUNET_CRYPTO_hash (castPtr @_ @() s) 1 porthash

        withCString "Opening CADET listen port\n" $ c'GNUNET_log_nocheck c'GNUNET_ERROR_TYPE_DEBUG
        void $ c'GNUNET_CADET_open_port mh porthash channelIncoming nullPtr nullFunPtr channelEnded nullPtr

        return ()
        )
