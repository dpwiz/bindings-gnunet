module HashSpec (spec) where

import Test.Hspec
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal
import Bindings.GNUnet.GnunetCryptoLib
import Bindings.GNUnet.GnunetCryptoTypes

spec :: Spec
spec = describe "hash" $ do
    -- FIXME
    (r1_, r2_, r3_) <- runIO $ do
        a <- newArray "a"
        let a_ = (castPtr a) :: Ptr ()

        b <- newArray "b"
        let b_ = (castPtr b) :: Ptr ()

        r1 <- malloc @C'GNUNET_HashCode
        r2 <- malloc @C'GNUNET_HashCode
        r3 <- malloc @C'GNUNET_HashCode

        c'GNUNET_CRYPTO_hash a_ 4 r1
        c'GNUNET_CRYPTO_hash a_ 4 r2
        c'GNUNET_CRYPTO_hash b_ 4 r3

        r1_ <- peek r1
        r2_ <- peek r2
        r3_ <- peek r3

        return (r1_, r2_, r3_)

    it "H(x) = H(x')" $ r1_ `shouldBe` r2_
    it "H(x') /= H(y)" $ r2_ `shouldNotBe` r3_
    it "H(x) /= H(y)" $ r1_ `shouldNotBe` r3_
