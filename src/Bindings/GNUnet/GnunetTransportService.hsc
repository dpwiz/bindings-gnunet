{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_transport_service.h"
module Bindings.GNUnet.GnunetTransportService where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetCryptoTypes (C'GNUNET_PeerIdentity)
import Bindings.GNUnet.GnunetTimeLib
import Bindings.GNUnet.GnunetConfigurationLib
import Bindings.GNUnet.GnunetSchedulerLib (C'GNUNET_SCHEDULER_TaskCallback)
import Bindings.GNUnet.GnunetHelloTypes

{- struct GNUNET_TRANSPORT_OfferHelloHandle; -}
#opaque_t struct GNUNET_TRANSPORT_OfferHelloHandle
#ccall GNUNET_TRANSPORT_offer_hello , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr <struct GNUNET_MessageHeader> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_TRANSPORT_OfferHelloHandle>)
#ccall GNUNET_TRANSPORT_offer_hello_cancel , Ptr <struct GNUNET_TRANSPORT_OfferHelloHandle> -> IO ()
{- struct GNUNET_TRANSPORT_AddressToStringContext; -}
#opaque_t struct GNUNET_TRANSPORT_AddressToStringContext
#callback GNUNET_TRANSPORT_AddressToStringCallback , Ptr () -> CString -> CInt -> IO ()
#ccall GNUNET_TRANSPORT_address_to_string , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr <struct GNUNET_HELLO_Address> -> CInt -> <struct GNUNET_TIME_Relative> -> <GNUNET_TRANSPORT_AddressToStringCallback> -> Ptr () -> IO (Ptr <struct GNUNET_TRANSPORT_AddressToStringContext>)
#ccall GNUNET_TRANSPORT_address_to_string_cancel , Ptr <struct GNUNET_TRANSPORT_AddressToStringContext> -> IO ()
{- enum GNUNET_TRANSPORT_PeerState {
    GNUNET_TRANSPORT_PS_NOT_CONNECTED = 0,
    GNUNET_TRANSPORT_PS_INIT_ATS,
    GNUNET_TRANSPORT_PS_SYN_SENT,
    GNUNET_TRANSPORT_PS_SYN_RECV_ATS,
    GNUNET_TRANSPORT_PS_SYN_RECV_ACK,
    GNUNET_TRANSPORT_PS_CONNECTED,
    GNUNET_TRANSPORT_PS_RECONNECT_ATS,
    GNUNET_TRANSPORT_PS_RECONNECT_SENT,
    GNUNET_TRANSPORT_PS_SWITCH_SYN_SENT,
    GNUNET_TRANSPORT_PS_DISCONNECT,
    GNUNET_TRANSPORT_PS_DISCONNECT_FINISHED
}; -}
#integral_t enum GNUNET_TRANSPORT_PeerState
#num GNUNET_TRANSPORT_PS_NOT_CONNECTED
#num GNUNET_TRANSPORT_PS_INIT_ATS
#num GNUNET_TRANSPORT_PS_SYN_SENT
#num GNUNET_TRANSPORT_PS_SYN_RECV_ATS
#num GNUNET_TRANSPORT_PS_SYN_RECV_ACK
#num GNUNET_TRANSPORT_PS_CONNECTED
#num GNUNET_TRANSPORT_PS_RECONNECT_ATS
#num GNUNET_TRANSPORT_PS_RECONNECT_SENT
#num GNUNET_TRANSPORT_PS_SWITCH_SYN_SENT
#num GNUNET_TRANSPORT_PS_DISCONNECT
#num GNUNET_TRANSPORT_PS_DISCONNECT_FINISHED
#ccall GNUNET_TRANSPORT_ps2s , <enum GNUNET_TRANSPORT_PeerState> -> IO CString
#ccall GNUNET_TRANSPORT_is_connected , <enum GNUNET_TRANSPORT_PeerState> -> IO CInt
{- struct GNUNET_TRANSPORT_PeerMonitoringContext; -}
#opaque_t struct GNUNET_TRANSPORT_PeerMonitoringContext
#callback GNUNET_TRANSPORT_PeerIterateCallback , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> Ptr <struct GNUNET_HELLO_Address> -> <enum GNUNET_TRANSPORT_PeerState> -> <struct GNUNET_TIME_Absolute> -> IO ()
#ccall GNUNET_TRANSPORT_monitor_peers , Ptr <struct GNUNET_CONFIGURATION_Handle> -> Ptr <struct GNUNET_PeerIdentity> -> CInt -> <GNUNET_TRANSPORT_PeerIterateCallback> -> Ptr () -> IO (Ptr <struct GNUNET_TRANSPORT_PeerMonitoringContext>)
#ccall GNUNET_TRANSPORT_monitor_peers_cancel , Ptr <struct GNUNET_TRANSPORT_PeerMonitoringContext> -> IO ()
{- struct GNUNET_TRANSPORT_Blacklist; -}
#opaque_t struct GNUNET_TRANSPORT_Blacklist
#callback GNUNET_TRANSPORT_BlacklistCallback , Ptr () -> Ptr <struct GNUNET_PeerIdentity> -> IO CInt
#ccall GNUNET_TRANSPORT_blacklist , Ptr <struct GNUNET_CONFIGURATION_Handle> -> <GNUNET_TRANSPORT_BlacklistCallback> -> Ptr () -> IO (Ptr <struct GNUNET_TRANSPORT_Blacklist>)
#ccall GNUNET_TRANSPORT_blacklist_cancel , Ptr <struct GNUNET_TRANSPORT_Blacklist> -> IO ()
{- struct GNUNET_TRANSPORT_PluginMonitor; -}
#opaque_t struct GNUNET_TRANSPORT_PluginMonitor
{- struct GNUNET_TRANSPORT_PluginSession; -}
#opaque_t struct GNUNET_TRANSPORT_PluginSession
{- enum GNUNET_TRANSPORT_SessionState {
    GNUNET_TRANSPORT_SS_INIT,
    GNUNET_TRANSPORT_SS_HANDSHAKE,
    GNUNET_TRANSPORT_SS_UP,
    GNUNET_TRANSPORT_SS_UPDATE,
    GNUNET_TRANSPORT_SS_DONE
}; -}
#integral_t enum GNUNET_TRANSPORT_SessionState
#num GNUNET_TRANSPORT_SS_INIT
#num GNUNET_TRANSPORT_SS_HANDSHAKE
#num GNUNET_TRANSPORT_SS_UP
#num GNUNET_TRANSPORT_SS_UPDATE
#num GNUNET_TRANSPORT_SS_DONE
{- struct GNUNET_TRANSPORT_SessionInfo {
    enum GNUNET_TRANSPORT_SessionState state;
    int is_inbound;
    uint32_t num_msg_pending;
    uint32_t num_bytes_pending;
    struct GNUNET_TIME_Absolute receive_delay;
    struct GNUNET_TIME_Absolute session_timeout;
    const struct GNUNET_HELLO_Address * address;
}; -}
#starttype struct GNUNET_TRANSPORT_SessionInfo
#field state , <enum GNUNET_TRANSPORT_SessionState>
#field is_inbound , CInt
#field num_msg_pending , CUInt
#field num_bytes_pending , CUInt
#field receive_delay , <struct GNUNET_TIME_Absolute>
#field session_timeout , <struct GNUNET_TIME_Absolute>
#field address , Ptr <struct GNUNET_HELLO_Address>
#stoptype
#callback GNUNET_TRANSPORT_SessionMonitorCallback , Ptr () -> Ptr <struct GNUNET_TRANSPORT_PluginSession> -> Ptr (Ptr ()) -> Ptr <struct GNUNET_TRANSPORT_SessionInfo> -> IO ()
#ccall GNUNET_TRANSPORT_monitor_plugins , Ptr <struct GNUNET_CONFIGURATION_Handle> -> <GNUNET_TRANSPORT_SessionMonitorCallback> -> Ptr () -> IO (Ptr <struct GNUNET_TRANSPORT_PluginMonitor>)
#ccall GNUNET_TRANSPORT_monitor_plugins_cancel , Ptr <struct GNUNET_TRANSPORT_PluginMonitor> -> IO ()
