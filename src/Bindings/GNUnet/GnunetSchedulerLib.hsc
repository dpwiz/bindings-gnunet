{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_scheduler_lib.h"
module Bindings.GNUnet.GnunetSchedulerLib where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCommon (C'GNUNET_SCHEDULER_Priority)
import Bindings.GNUnet.GnunetTimeLib
import Bindings.GNUnet.GnunetDiskLib (C'GNUNET_DISK_FileHandle)
import Bindings.GNUnet.GnunetNetworkLib
{- struct GNUNET_SCHEDULER_Task; -}
#opaque_t struct GNUNET_SCHEDULER_Task
{- enum GNUNET_SCHEDULER_Reason {
    GNUNET_SCHEDULER_REASON_NONE = 0,
    GNUNET_SCHEDULER_REASON_STARTUP = 1,
    GNUNET_SCHEDULER_REASON_SHUTDOWN = 2,
    GNUNET_SCHEDULER_REASON_TIMEOUT = 4,
    GNUNET_SCHEDULER_REASON_READ_READY = 8,
    GNUNET_SCHEDULER_REASON_WRITE_READY = 16,
    GNUNET_SCHEDULER_REASON_PREREQ_DONE = 32
}; -}
#integral_t enum GNUNET_SCHEDULER_Reason
#num GNUNET_SCHEDULER_REASON_NONE
#num GNUNET_SCHEDULER_REASON_STARTUP
#num GNUNET_SCHEDULER_REASON_SHUTDOWN
#num GNUNET_SCHEDULER_REASON_TIMEOUT
#num GNUNET_SCHEDULER_REASON_READ_READY
#num GNUNET_SCHEDULER_REASON_WRITE_READY
#num GNUNET_SCHEDULER_REASON_PREREQ_DONE
{- enum GNUNET_SCHEDULER_EventType {
    GNUNET_SCHEDULER_ET_NONE = 0,
    GNUNET_SCHEDULER_ET_IN = 1,
    GNUNET_SCHEDULER_ET_OUT = 2,
    GNUNET_SCHEDULER_ET_HUP = 4,
    GNUNET_SCHEDULER_ET_ERR = 8,
    GNUNET_SCHEDULER_ET_PRI = 16,
    GNUNET_SCHEDULER_ET_NVAL = 32
}; -}
#integral_t enum GNUNET_SCHEDULER_EventType
#num GNUNET_SCHEDULER_ET_NONE
#num GNUNET_SCHEDULER_ET_IN
#num GNUNET_SCHEDULER_ET_OUT
#num GNUNET_SCHEDULER_ET_HUP
#num GNUNET_SCHEDULER_ET_ERR
#num GNUNET_SCHEDULER_ET_PRI
#num GNUNET_SCHEDULER_ET_NVAL
{- struct GNUNET_SCHEDULER_FdInfo {
    const struct GNUNET_NETWORK_Handle * fd;
    const struct GNUNET_DISK_FileHandle * fh;
    enum GNUNET_SCHEDULER_EventType et;
    int sock;
}; -}
#starttype struct GNUNET_SCHEDULER_FdInfo
#field fd , Ptr <struct GNUNET_NETWORK_Handle>
#field fh , Ptr <struct GNUNET_DISK_FileHandle>
#field et , <enum GNUNET_SCHEDULER_EventType>
#field sock , CInt
#stoptype
{- struct GNUNET_SCHEDULER_TaskContext {
    enum GNUNET_SCHEDULER_Reason reason;
    unsigned int fds_len;
    const struct GNUNET_SCHEDULER_FdInfo * fds;
    const struct GNUNET_NETWORK_FDSet * read_ready;
    const struct GNUNET_NETWORK_FDSet * write_ready;
}; -}
#starttype struct GNUNET_SCHEDULER_TaskContext
#field reason , <enum GNUNET_SCHEDULER_Reason>
#field fds_len , CUInt
#field fds , Ptr <struct GNUNET_SCHEDULER_FdInfo>
#field read_ready , Ptr <struct GNUNET_NETWORK_FDSet>
#field write_ready , Ptr <struct GNUNET_NETWORK_FDSet>
#stoptype
#ccall GNUNET_SCHEDULER_task_ready , Ptr <struct GNUNET_SCHEDULER_Task> -> Ptr <struct GNUNET_SCHEDULER_FdInfo> -> IO ()
{- struct GNUNET_SCHEDULER_Handle; -}
#opaque_t struct GNUNET_SCHEDULER_Handle
#ccall GNUNET_SCHEDULER_do_work , Ptr <struct GNUNET_SCHEDULER_Handle> -> IO CInt
{- struct GNUNET_SCHEDULER_Driver {
    void * cls;
    int (* add)(void * cls,
                struct GNUNET_SCHEDULER_Task * task,
                struct GNUNET_SCHEDULER_FdInfo * fdi);
    int (* del)(void * cls, struct GNUNET_SCHEDULER_Task * task);
    void (* set_wakeup)(void * cls, struct GNUNET_TIME_Absolute dt);
}; -}
#callback GNUNET_SCHEDULER_Driver_add , Ptr () -> Ptr <struct GNUNET_SCHEDULER_Task> -> Ptr <struct GNUNET_SCHEDULER_FdInfo> -> IO CInt
#callback GNUNET_SCHEDULER_Driver_del , Ptr () -> Ptr <struct GNUNET_SCHEDULER_Task> -> IO CInt
#callback GNUNET_SCHEDULER_Driver_set_wakeup , Ptr () -> <GNUNET_TIME_Absolute> -> IO ()
#starttype struct GNUNET_SCHEDULER_Driver
#field cls , Ptr ()
#field add , <GNUNET_SCHEDULER_Driver_add>
#field del , <GNUNET_SCHEDULER_Driver_del>
#field set_wakeup , <GNUNET_SCHEDULER_Driver_set_wakeup>
#stoptype
#callback GNUNET_SCHEDULER_TaskCallback , Ptr () -> IO ()
#ccall GNUNET_SCHEDULER_driver_init , Ptr <struct GNUNET_SCHEDULER_Driver> -> IO (Ptr <struct GNUNET_SCHEDULER_Handle>)
#ccall GNUNET_SCHEDULER_driver_done , Ptr <struct GNUNET_SCHEDULER_Handle> -> IO ()
#ccall GNUNET_SCHEDULER_driver_select , IO (Ptr <struct GNUNET_SCHEDULER_Driver>)
#callback GNUNET_SCHEDULER_select , Ptr () -> Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_FDSet> -> <GNUNET_TIME_Relative> -> IO CInt
#ccall GNUNET_SCHEDULER_run , <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO ()
#ccall GNUNET_SCHEDULER_shutdown , IO ()
#ccall GNUNET_SCHEDULER_get_load , <enum GNUNET_SCHEDULER_Priority> -> IO CUInt
#ccall GNUNET_SCHEDULER_get_task_context , IO (Ptr <struct GNUNET_SCHEDULER_TaskContext>)
#ccall GNUNET_SCHEDULER_cancel , Ptr <struct GNUNET_SCHEDULER_Task> -> IO (Ptr ())
#ccall GNUNET_SCHEDULER_add_with_reason_and_priority , <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> <enum GNUNET_SCHEDULER_Reason> -> <enum GNUNET_SCHEDULER_Priority> -> IO ()
#ccall GNUNET_SCHEDULER_add_with_priority , <enum GNUNET_SCHEDULER_Priority> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_now , <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_shutdown , <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_now_with_lifeness , CInt -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_delayed , <GNUNET_TIME_Relative> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_at , <GNUNET_TIME_Absolute> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_delayed_with_priority , <GNUNET_TIME_Relative> -> <enum GNUNET_SCHEDULER_Priority> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_at_with_priority , <GNUNET_TIME_Absolute> -> <enum GNUNET_SCHEDULER_Priority> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_read_net , <GNUNET_TIME_Relative> -> Ptr <struct GNUNET_NETWORK_Handle> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_read_net_with_priority , <GNUNET_TIME_Relative> -> <enum GNUNET_SCHEDULER_Priority> -> Ptr <struct GNUNET_NETWORK_Handle> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_write_net , <GNUNET_TIME_Relative> -> Ptr <struct GNUNET_NETWORK_Handle> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_net_with_priority , <GNUNET_TIME_Relative> -> <enum GNUNET_SCHEDULER_Priority> -> Ptr <struct GNUNET_NETWORK_Handle> -> CInt -> CInt -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_read_file , <GNUNET_TIME_Relative> -> Ptr <struct GNUNET_DISK_FileHandle> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_write_file , <GNUNET_TIME_Relative> -> Ptr <struct GNUNET_DISK_FileHandle> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_file_with_priority , <GNUNET_TIME_Relative> -> <enum GNUNET_SCHEDULER_Priority> -> Ptr <struct GNUNET_DISK_FileHandle> -> CInt -> CInt -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_add_select , <enum GNUNET_SCHEDULER_Priority> -> <GNUNET_TIME_Relative> -> Ptr <struct GNUNET_NETWORK_FDSet> -> Ptr <struct GNUNET_NETWORK_FDSet> -> <GNUNET_SCHEDULER_TaskCallback> -> Ptr () -> IO (Ptr <struct GNUNET_SCHEDULER_Task>)
#ccall GNUNET_SCHEDULER_set_select , <GNUNET_SCHEDULER_select> -> Ptr () -> IO ()
