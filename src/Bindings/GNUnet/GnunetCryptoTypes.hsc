{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_crypto_lib.h"
module Bindings.GNUnet.GnunetCryptoTypes where
import Foreign.Ptr
#strict_import

{- struct GNUNET_HashCode {
    uint32_t bits[512 / 8 / sizeof(uint32_t)];
}; -}
#starttype struct GNUNET_HashCode
#array_field bits , CUInt
#stoptype
{- struct GNUNET_ShortHashCode {
    uint32_t bits[256 / 8 / sizeof(uint32_t)];
}; -}
#starttype struct GNUNET_ShortHashCode
#array_field bits , CUInt
#stoptype
{- enum GNUNET_CRYPTO_Quality {
    GNUNET_CRYPTO_QUALITY_WEAK,
    GNUNET_CRYPTO_QUALITY_STRONG,
    GNUNET_CRYPTO_QUALITY_NONCE
}; -}
#integral_t enum GNUNET_CRYPTO_Quality
#num GNUNET_CRYPTO_QUALITY_WEAK
#num GNUNET_CRYPTO_QUALITY_STRONG
#num GNUNET_CRYPTO_QUALITY_NONCE
{- struct GNUNET_CRYPTO_HashAsciiEncoded {
    unsigned char encoding[104];
}; -}
#starttype struct GNUNET_CRYPTO_HashAsciiEncoded
#array_field encoding , CUChar
#stoptype
{- struct GNUNET_CRYPTO_EccSignaturePurpose {
    uint32_t size __attribute__((packed));
    uint32_t purpose __attribute__((packed));
}; -}
#starttype struct GNUNET_CRYPTO_EccSignaturePurpose
#field size , CUInt
#field purpose , CUInt
#stoptype
{- struct GNUNET_CRYPTO_EddsaSignature {
    unsigned char r[256 / 8]; unsigned char s[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EddsaSignature
#array_field r , CUChar
#array_field s , CUChar
#stoptype
{- struct GNUNET_CRYPTO_EcdsaSignature {
    unsigned char r[256 / 8]; unsigned char s[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EcdsaSignature
#array_field r , CUChar
#array_field s , CUChar
#stoptype
{- struct GNUNET_CRYPTO_EddsaPublicKey {
    unsigned char q_y[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EddsaPublicKey
#array_field q_y , CUChar
#stoptype
{- struct GNUNET_CRYPTO_EcdsaPublicKey {
    unsigned char q_y[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EcdsaPublicKey
#array_field q_y , CUChar
#stoptype
{- struct GNUNET_PeerIdentity {
    struct GNUNET_CRYPTO_EddsaPublicKey public_key;
}; -}
#starttype struct GNUNET_PeerIdentity
#field public_key , <struct GNUNET_CRYPTO_EddsaPublicKey>
#stoptype
{- struct GNUNET_CRYPTO_EcdhePublicKey {
    unsigned char q_y[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EcdhePublicKey
#array_field q_y , CUChar
#stoptype
{- struct GNUNET_CRYPTO_EcdhePrivateKey {
    unsigned char d[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EcdhePrivateKey
#array_field d , CUChar
#stoptype
{- struct GNUNET_CRYPTO_EcdsaPrivateKey {
    unsigned char d[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EcdsaPrivateKey
#array_field d , CUChar
#stoptype
{- struct GNUNET_CRYPTO_EddsaPrivateKey {
    unsigned char d[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_EddsaPrivateKey
#array_field d , CUChar
#stoptype
{- struct GNUNET_CRYPTO_SymmetricSessionKey {
    unsigned char aes_key[256 / 8]; unsigned char twofish_key[256 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_SymmetricSessionKey
#array_field aes_key , CUChar
#array_field twofish_key , CUChar
#stoptype
{- struct GNUNET_CRYPTO_SymmetricInitializationVector {
    unsigned char aes_iv[256 / 8 / 2];
    unsigned char twofish_iv[256 / 8 / 2];
}; -}
#starttype struct GNUNET_CRYPTO_SymmetricInitializationVector
#array_field aes_iv , CUChar
#array_field twofish_iv , CUChar
#stoptype
{- struct GNUNET_CRYPTO_AuthKey {
    unsigned char key[512 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_AuthKey
#array_field key , CUChar
#stoptype
{- struct GNUNET_CRYPTO_PaillierPublicKey {
    unsigned char n[2048 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_PaillierPublicKey
#array_field n , CUChar
#stoptype
{- struct GNUNET_CRYPTO_PaillierPrivateKey {
    unsigned char lambda[2048 / 8]; unsigned char mu[2048 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_PaillierPrivateKey
#array_field lambda , CUChar
#array_field mu , CUChar
#stoptype
{- struct GNUNET_CRYPTO_PaillierCiphertext {
    int32_t remaining_ops __attribute__((packed));
    unsigned char bits[2048 * 2 / 8];
}; -}
#starttype struct GNUNET_CRYPTO_PaillierCiphertext
#field remaining_ops , CInt
#array_field bits , CUChar
#stoptype

