{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include "gnunet/gnunet_hello_lib.h"
module Bindings.GNUnet.GnunetHelloLib where
import Foreign.Ptr
#strict_import

import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetCryptoTypes (C'GNUNET_PeerIdentity, C'GNUNET_CRYPTO_EddsaPublicKey)
import Bindings.GNUnet.GnunetTimeLib (C'GNUNET_TIME_Absolute)
import Bindings.GNUnet.GnunetTransportPlugin (C'GNUNET_TRANSPORT_PluginFunctions)
import Bindings.GNUnet.GnunetHelloTypes

#ccall GNUNET_HELLO_address_allocate , Ptr <struct GNUNET_PeerIdentity> -> CString -> Ptr () -> CSize -> <enum GNUNET_HELLO_AddressInfo> -> IO (Ptr <struct GNUNET_HELLO_Address>)
#ccall GNUNET_HELLO_address_copy , Ptr <struct GNUNET_HELLO_Address> -> IO (Ptr <struct GNUNET_HELLO_Address>)
#ccall GNUNET_HELLO_address_cmp , Ptr <struct GNUNET_HELLO_Address> -> Ptr <struct GNUNET_HELLO_Address> -> IO CInt
#ccall GNUNET_HELLO_address_get_size , Ptr <struct GNUNET_HELLO_Address> -> IO CSize
#ccall GNUNET_HELLO_address_check_option , Ptr <struct GNUNET_HELLO_Address> -> <enum GNUNET_HELLO_AddressInfo> -> IO CInt
#ccall GNUNET_HELLO_is_friend_only , Ptr <struct GNUNET_HELLO_Message> -> IO CInt
#ccall GNUNET_HELLO_add_address , Ptr <struct GNUNET_HELLO_Address> -> <struct GNUNET_TIME_Absolute> -> CString -> CSize -> IO CSize
#callback GNUNET_HELLO_GenerateAddressListCallback , Ptr () -> CSize -> Ptr () -> IO CLong
#ccall GNUNET_HELLO_create , Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> <GNUNET_HELLO_GenerateAddressListCallback> -> Ptr () -> CInt -> IO (Ptr <struct GNUNET_HELLO_Message>)
#ccall GNUNET_HELLO_size , Ptr <struct GNUNET_HELLO_Message> -> IO CUShort
#ccall GNUNET_HELLO_merge , Ptr <struct GNUNET_HELLO_Message> -> Ptr <struct GNUNET_HELLO_Message> -> IO (Ptr <struct GNUNET_HELLO_Message>)
#ccall GNUNET_HELLO_equals , Ptr <struct GNUNET_HELLO_Message> -> Ptr <struct GNUNET_HELLO_Message> -> <struct GNUNET_TIME_Absolute> -> IO (<struct GNUNET_TIME_Absolute>)
#callback GNUNET_HELLO_AddressIterator , Ptr () -> Ptr <struct GNUNET_HELLO_Address> -> <struct GNUNET_TIME_Absolute> -> IO CInt
#ccall GNUNET_HELLO_get_last_expiration , Ptr <struct GNUNET_HELLO_Message> -> IO (<struct GNUNET_TIME_Absolute>)
#ccall GNUNET_HELLO_iterate_addresses , Ptr <struct GNUNET_HELLO_Message> -> CInt -> <GNUNET_HELLO_AddressIterator> -> Ptr () -> IO (Ptr <struct GNUNET_HELLO_Message>)
#ccall GNUNET_HELLO_iterate_new_addresses , Ptr <struct GNUNET_HELLO_Message> -> Ptr <struct GNUNET_HELLO_Message> -> <struct GNUNET_TIME_Absolute> -> <GNUNET_HELLO_AddressIterator> -> Ptr () -> IO ()
#ccall GNUNET_HELLO_get_id , Ptr <struct GNUNET_HELLO_Message> -> Ptr <struct GNUNET_PeerIdentity> -> IO CInt
#ccall GNUNET_HELLO_get_header , Ptr <struct GNUNET_HELLO_Message> -> IO (Ptr <struct GNUNET_MessageHeader>)
#callback GNUNET_HELLO_TransportPluginsFind , CString -> IO (Ptr <struct GNUNET_TRANSPORT_PluginFunctions>)
#ccall GNUNET_HELLO_compose_uri , Ptr <struct GNUNET_HELLO_Message> -> <GNUNET_HELLO_TransportPluginsFind> -> IO CString
#ccall GNUNET_HELLO_parse_uri , CString -> Ptr <struct GNUNET_CRYPTO_EddsaPublicKey> -> Ptr (Ptr <struct GNUNET_HELLO_Message>) -> <GNUNET_HELLO_TransportPluginsFind> -> IO CInt
